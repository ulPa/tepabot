import logging
from pyrogram import Client
logging.basicConfig(format=' %(message)s',
                    level=logging.INFO)

app = Client('TePaBot', plugins={'root': 'plugins'})

app.run()
