# Plugins
These are the plugins I wrote or 'stole' and changed them to my needs. I took them from these sources:
* [Pyrogram Plugin Repository](https://github.com/pyrogram/plugins/)
* (soon) [ColinSharks Pyrobot](https://git.colinshark.de/pyrobot/pyrobot)
## Here is a list of Configurations for each plugin
Please add them to the config.ini file, altough they all work without any settings, I think it is easier to remember the syntax, when you choose it on your own.
### haste.py
With this plugin, you can put messages into a hastebin. To use it, just reply to a message or put it after the command.
#### Configurations
```
[haste]
command = h
base = haste.thevillage.chat
``` 
#### Syntax
{prefix}{command} [-m] [-p] [code]
-m uses the specified code
-p sends the link in private