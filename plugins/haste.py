import sys
import requests
import configparser
from pyrogram import Client, Filters
from bot_log import log

config = configparser.ConfigParser()
config.read('config.ini')
plugin_settings = config['haste']
global_settings = config['tepabot']
pf = '.' if 'prefix' not in global_settings else global_settings['prefix']
base = 'https://haste.thevillage.chat' if 'base' not in plugin_settings else plugin_settings['base']


def delete(origin, what):
    for piece in what:
        origin = origin.replace(piece, '')
    return origin


@Client.on_message(Filters.me & Filters.command(plugin_settings['command'], pf))
def haste(client, message):
    try:
        def create_haste(text):
            return base+'/'+requests.post("{}/documents".format(base),
                                          data=text.encode("UTF-8")).json()['key']
        commands = message.text.split()
        edit = False if global_settings['command']+plugin_settings['send_private'] in commands else True
        add = '' if global_settings['command']+plugin_settings['add'] not in commands else \
            message.text.markdown.split(global_settings['command']+plugin_settings['add'])[1]
        link = create_haste(message.reply_to_message.text if message.reply_to_message and
                            global_settings['command']+plugin_settings['use_message'] not in message.text else
                            delete(message.text, [pf+plugin_settings['command'],
                                                  global_settings['command']+plugin_settings['send_private'],
                                                  global_settings['command']+plugin_settings['use_message']]).
                            split(global_settings['command']+plugin_settings['add'])[0])
        if edit:
            message.edit_text(link+' '+add)
        else:
            message.delete()
            client.send_message(chat_id='me',
                                text=link+' '+add)
    except:
        e = sys.exc_info()[0]
        log('**While Handling** \n __{}__ \n `{}` \n **occured**'.format(message.text.markdown, e))
        raise
